import { Box } from 'grommet'
import Img from 'gatsby-image'
import React from 'react'
import shortid from 'shortid'

const Icons = ({ data }) => {
  return (
    <Box direction="row-responsive" gap="medium">
      {Object.keys(data).map((icon, index) => {
        const imagePath = data[`${icon}`].childImageSharp.fluid
        return (
          <Box width="xlarge" align="center" key={shortid.generate()}>
            <Img
              alt={icon}
              key={index}
              style={{ width: '42px', height: '42px' }}
              fluid={imagePath}
            />
          </Box>
        )
      })}
    </Box>
  )
}

export default Icons
