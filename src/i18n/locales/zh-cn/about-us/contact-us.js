module.exports = {
  title: '联系我们',
  'meta-description': '联系我们',
  banner: {
    title: '联系我们',
    backgroundImage: 'bannerGlobe',
    description: "与我们合作",
    buttons: {
      number: '0',
    },
  },
  contact: {
    number: '1',
    columns0: {
      title: 'null',
      description: {
        richText: 'null',
        description: 'null',
      },
      number: '4',
      columns: '4',
      buttons: {
        number: '0',
      },
      backgroundImage: 'null',
      column0: {
        images: {
          number: '0',
        },
        title: '一般查询',
        description: {
          richText: 'null',
          description: '一般信息，请发送邮件给',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'contact@cybermiles.io',
            link: 'mailto:contact@cybermiles.io',
            icon: 'null',
          },
        },
      },
      column1: {
        images: {
          number: '0',
        },
        title: '商业合作伙伴',
        description: {
          richText: 'null',
          description: '如果您有兴趣与我们合作，请发送邮件给',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'business@cybermiles.io.',
            link: 'mailto:business@cybermiles.io',
            icon: 'null',
          },
        },
      },
      column2: {
        images: {
          number: '0',
        },
        title: '开发者支持',
        description: {
          richText: 'null',
          description: '如果你是开发者并且对技术有问题，请访问',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'forum.cybermiles.io',
            link: 'https://forum.cybermiles.io/',
            icon: 'null',
          },
        },
      },
      column3: {
        images: {
          number: '0',
        },
        title: '媒体',
        description: {
          richText: 'null',
          description: '媒体咨询和访问，请发送邮件给',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'media@cybermiles.io',
            link: 'mailto:media@cybermiles.io',
            icon: 'null',
          },
        },
      },
    },
  },
  social: {
    number: '1',
    columns0: {
      title: '加入CyberMiles公链全球社区',
      description: {
        richText: 'null',
        description: 'null',
      },
      number: '12',
      columns: '6',
      buttons: {
        number: '0',
      },
      backgroundImage: 'shortLastSection',
      column0: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'twitter',
            link: 'https://twitter.com/cybermiles',
            icon: 'twitter',
          },
        },
      },
      column1: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'facebook',
            link: 'https://www.facebook.com/cybermiles',
            icon: 'facebook',
          },
        },
      },
      column2: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'weibo',
            link: 'https://www.weibo.com/Cybermiles',
            icon: 'weibo',
          },
        },
      },
      column3: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'wechat',
            link: '/',
            icon: 'wechat',
          },
        },
      },
      column4: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'telegram',
            link: 'https://t.me/cybermilestoken',
            icon: 'telegram',
          },
        },
      },
      column5: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'kakaotalk',
            link: 'https://open.kakao.com/o/gl9gPJI',
            icon: 'kakao_talk',
          },
        },
      },
      column6: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'github',
            link: 'https://github.com/cybermiles',
            icon: 'github',
          },
        },
      },
      column7: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'reddit',
            link: 'https://www.reddit.com/r/CyberMiles/',
            icon: 'reddit',
          },
        },
      },
      column8: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'youtube',
            link: 'https://www.youtube.com/channel/UCgok7sGPWbxKAkz2ts9etNg/featured',
            icon: 'youtube',
          },
        },
      },
      column9: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'linkedin',
            link: 'https://www.linkedin.com/company/cybermiles/',
            icon: 'linkedin',
          },
        },
      },
      column10: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'medium',
            link: 'https://medium.com/cybermiles',
            icon: 'medium',
          },
        },
      },
      column11: {
        images: {
          number: '0',
        },
        title: 'null',
        description: {
          richText: 'null',
          description: 'null',
        },
        buttons: {
          number: '1',
          button0: {
            label: 'naver blog',
            link: 'https://blog.naver.com/cybermileskr',
            icon: 'blog',
          },
        },
      },
    },
  },
}
