module.exports = {
  title: 'RoadMap',
  'meta-description': 'RoadMap',
  banner: {
    title: 'RoadMap',
    backgroundImage: 'bannerGlobe',
    description: 'null',
    buttons: {
      number: '0',
    },
  },
  roadmap: {
    title: 'null',
    description: {
      richtext: 'null',
      description:
        'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '0',
    },
    events: {
      number: '12',
      // event0: {
      //   status: 'Past',
      //   date: '2018 Dec',
      //   description:
      //     'Supports multiple real-world e-commerce scenarios including CMT payments on blocktonic, Lightinthebox, and Dallas Mavericks tickets',
      // },
      event0: {
        status: 'Future',
        date: '2019 Jan',
        description:
          'Completes a minimal viable ecosystem (MVE) for on-chain tokens and exchanges',
      },
      event1: {
        status: 'Future',
        date: '2019 Feb',
        description:
          'Incorporates over 15M American consumers’ data onto the CyberMiles public blockchain',
      },
      event2: {
        status: 'Future',
        date: '2019 April',
        description: 'Supports over 100 DApps',
      },
      event3: {
        status: 'Future',
        date: '2019 June',
        description:
          'Onboards first 50 business partners for e-commerce applications',
      },
      event4: {
        status: 'Future',
        date: '2019 July',
        description:
          'Sustains over twice as many daily transactions as Ethereum',
      },
      event5: {
        status: 'Future',
        date: '2019 August',
        description: 'Reaches 2000 community Developers',
      },
      event6: {
        status: 'Future',
        date: '2019 September',
        description: 'Reaches more DAU than Ethereum and EOS combined',
      },
      event7: {
        status: 'Future',
        date: '2019 October',
        description: 'Supports stable coins in all e-commerce transactions',
      },
      event8: {
        status: 'Future',
        date: '2019 Q4',
        description:
          'Becomes one of the top Security Tokens issuers in the world',
      },
      event9: {
        status: 'Future',
        date: '2020 Q1',
        description:
          'Receives over 100 e-commerce virtual machine optimizations from the developer community',
      },
      event10: {
        status: 'Future',
        date: '2020 Q2',
        description:
          'Launch global enterprise services to promote public blockchain as a service',
      },
      event11: {
        status: 'Future',
        date: '2020 Q3',
        description:
          'Becomes a top e-commerce transaction clearing house in South Asia, Southeast Asia North East Asia and Africa',
      },
      event12: {
        status: 'Future',
        date: '2020 Q4',
        description: 'Reaches massive adoption of decentralized e-commerce',
      },
    },
  },
}
