module.exports = {
  title: 'MetaMask for CMT',
  'meta-description': 'Welcome to MetaMask for CMT',
  banner: {
    title: 'MetaMask for CMT',
    backgroundImage: 'bannerCMT',
    description: 'null',
    buttons: {
      number: '0',
    },
  },
metamask: {
    number: '1',
    centered0: {
      images: {
        number: '1',
        image0: {
          gif: 'null',
          path: 'metamask',
          link: 'null',
        },
      },
      title: 'Bring CyberMiles public blockchain to your browser',
      description: {
        richText: 'true',
        description:
          `<p>MetaMask for CMT” is a bridge that allows you to visit the distributed web of tomorrow in your browser today. It allows you to run CyberMiles dApps right in your browser without running a CyberMiles node.</p><p>MetaMask includes a secure identity vault, providing a user interface to manage your identities on different sites and sign blockchain transactions.</p><p>You can install the MetaMask add-on in Chrome browser. If you’re a developer, you can start developing with MetaMask today.</p>`,
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          link:
            'https://chrome.google.com/webstore/detail/metamask-for-cmt/hmiddckbbijmdkamphkgkelnjjdkicck',
          label: 'Get Chrome Extension',
          icon: 'null',
        },
      },
    },
  },
  columns: {
    number: '1',
    columns0: {
      number: '3',
      columns: '3',
      title: 'Installation Guide',
      description: {
        richText: 'null',
        description: 'null',
      },
      buttons: {
        number: '0',
      },
      backgroundImage: 'shortLastSection',
      column0: {
        image: {
          gif: 'null',
          path: 'null',
          link: 'null',
        },
        title: '1',
        description: {
          richtext: 'null',
          description: 'Download the “metamask4cmt.crx” file',
        },
        buttons: {
          number: '0',
        },
      },
      column1: {
        image: {
          gif: 'null',
          path: 'null',
          link: 'null',
        },
        title:
          '2',
        description: {
          richtext: 'null',
          description: 'Open the Chrome browser “Extensions” and enable the developer mode',
        },
        buttons: {
          number: '0',
        },
      },
      column2: {
        image: {
          gif: 'null',
          path: 'null',
          link: 'null',
        },
        title:
          '3',
        description: {
          richtext: 'null',
          description: 'Drag and drop the “metamask4cmt.crx” file to the extension interface',
        },
        buttons: {
          number: '0',
        },
      },
    },
  },
}
