module.exports = {
  title: 'Cybermiles',
  'meta-description':
    'CyberMiles is a smart, fast, secure, and free blockchain platform built for e-commerce with lightning-fast transaction speed and smart contract templates.',
  banner: {
    title: 'CyberMiles',
    backgroundImage: 'bannerCube',
    description: '전자 상거래 블록체인',
    buttons: {
      number: '1',
      button0: {
        label: 'CyberMiles가 무엇인가요?',
        link: '/',
        icon: 'play',
      },
    },
  },
  imageText: {
    section0: {
      image: {
        gif: 'true',
        path: 'cube',
      },
      title: '전자 상거래의 베스트 솔루션',
      description: {
        richtext: 'null',
        description:
          'CyberMiles는 전자 상거래 기업들이 블록체인에 비즈니스를 쉽게 배포 할 수 있도록 고도로 맞춤화된 솔루션을 구축했습니다.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '유즈 케이스',
          link: 'ecommerce-solutions/finance/payment-gateway/',
        },
      },
    },
    section1: {
      image: {
        gif: 'true',
        path: 'coin',
      },
      title: '가치있는 블록체인 기술',
      description: {
        richtext: 'null',
        description:
          ' CyberMiles 는 스마트하고, 빠르고 안전한 블록체인 네트워크 입니다. 모든 유저에게 무료 입니다.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: 'CMT 구매',
          link: 'cmt/overview/',
        },
      },
    },
    section2: {
      image: {
        gif: 'true',
        path: 'lity',
      },
      title: '블록체인에 간단하게 개발하기',
      description: {
        richtext: 'null',
        description:
          'CyberMiles는 디앱 개발을 위해 강력한 인프라를 구축했습니다.',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '사이버마일즈로 개발',
          link: 'developer-portal/developer-hub',
        },
      },
    },
  },
  roadmap: {
    title: '로드맵',
    description: {
      richtext: 'null',
      description: 'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '1',
      button0: {
        label: '더 알아보기',
        link: '/about-us/roadmap',
      },
    },
    events: {
      number: '4',
      event0: {
        status: 'Future',
        date: '2019년 1월 ',
        description:
          '온 체인 토큰과 거래소를 위한 최소 실행 가능한 생태계 (Minimal Viable Ecosystem) 완성',
      },
      event1: {
        status: 'Future',
        date: '2019년 2월',
        description:
          '1천 5백만 이상의 미국 소비자 데이터를 사이버마일즈 블록체인으로 통합',
      },
      event2: {
        status: 'Future',
        date: '2019년 4월',
        description: '100개 이상의 dApp 지원',
      },
      event3: {
        status: 'Future',
        date: '2019년 6월',
        description:
          '전자 상거래 애플리케이션을 위한 첫 50개의 비즈니스 파트너 협약',
      },
    },
  },
  centered: {
    centered0: {
      image: 'null',
      title: 'CyberMiles 와 함께 시작합시다 ',
      description: {
        richText: 'null',
        description:
          '좋은 아이디어 있으세요?저희와 공유해주세요!',
      },
      backgroundImage: 'null',
      buttons: {
        number: '1',
        button0: {
          label: '연락처',
          link: '/about-us/contact-us',
        },
      },
    },
  },
}
