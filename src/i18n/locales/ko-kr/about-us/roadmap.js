module.exports = {
  title: 'RoadMap',
  'meta-description': 'RoadMap',
  banner: {
    title: '로드맵',
    backgroundImage: 'bannerGlobe',
    description: 'null',
    buttons: {
      number: '0',
    },
  },
  roadmap: {
    title: 'null',
    description: {
      richtext: 'null',
      description:
        'null',
    },
    backgroundImage: 'lastSection',
    buttons: {
      number: '0',
    },
    events: {
      number: '12',
      // event0: {
      //   status: 'Past',
      //   date: '2018 Dec',
      //   description:
      //     'Supports multiple real-world e-commerce scenarios including CMT payments on blocktonic, Lightinthebox, and Dallas Mavericks tickets',
      // },
      event0: {
        status: 'Future',
        date: '2019년 1월 ',
        description:
          '온 체인 토큰과 거래소를 위한 최소 실행 가능한 생태계 (Minimal Viable Ecosystem) 완성',
      },
      event1: {
        status: 'Future',
        date: '2019년 2월',
        description:
          '1천 5백만 이상의 미국 소비자 데이터를 사이버마일즈 블록체인으로 통합',
      },
      event2: {
        status: 'Future',
        date: '2019년 4월',
        description: '100개 이상의 dApp 지원',
      },
      event3: {
        status: 'Future',
        date: '2019년 6월',
        description:
          '전자 상거래 애플리케이션을 위한 첫 50개의 비즈니스 파트너 협약',
      },
      event4: {
        status: 'Future',
        date: '2019년 7월',
        description:
          '이더리움보다 2배 많은 일일거래량 유지',
      },
      event5: {
        status: 'Future',
        date: '2019년 8월',
        description: '2,000명의 커뮤니티 개발자 보유',
      },
      event6: {
        status: 'Future',
        date: '2019년 9월 ',
        description: '이더리움과 EOS를 합친것 보다 많은 활성 사용자 보유',
      },
      event7: {
        status: 'Future',
        date: '2019년 10월',
        description: '모든 전자 상거래에 스테이블 코인 (Stable Coin) 지원',
      },
      event8: {
        status: 'Future',
        date: '2019년 4분기',
        description:
          '세계의 탑 Security Token 발행자 안에 들기',
      },
      event9: {
        status: 'Future',
        date: '2020년 1분기',
        description:
          '개발자 커뮤니티에 의해 100개 이상의 전자 상거래 가상 머신 (Virtual Machine) 최적화',
      },
      event10: {
        status: 'Future',
        date: '2020년 2분기',
        description:
          '글로벌 기업 서비스를 론칭해 퍼블릭 블록체인을 서비스로 홍보',
      },
      event11: {
        status: 'Future',
        date: '2020년 3분기',
        description:
          '극동아시아, 동남아시아, 남아시아, 그리고 아프리카에서의 탑 전자 상거래를 위한 기관 달성',
      },
      event12: {
        status: 'Future',
        date: '2020년 4분기',
        description: '분산화된 전자 상거래의 시대',
      },
    },
  },
}
